#!/usr/bin/env nextflow

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

include { procd_fqs_to_alns } from '../alignment/alignment.nf'

include { samtools_index } from '../samtools/samtools.nf'
include { samtools_sort } from '../samtools/samtools.nf'
include { samtools_view } from '../samtools/samtools.nf'

// CNA tools
// Sequenza
include { sequenza_gc_wiggle } from '../sequenza/sequenza.nf'
include { sequenza_bam2seqz } from '../sequenza/sequenza.nf'
include { sequenza_seqz_binning } from '../sequenza/sequenza.nf'
include { sequenza_extract } from '../sequenza/sequenza.nf'
include { sequenza_fit }  from '../sequenza/sequenza.nf'
include { sequenza_result } from '../sequenza/sequenza.nf'
// CNVkit
include { cnvkit_batch } from '../cnvkit/cnvkit.nf'

// CCF tools
// PyClone-VI
include { lenstools_make_pyclonevi_inputs } from '../lenstools/lenstools.nf'
include { pyclonevi_fit } from '../pyclone-vi/pyclone-vi.nf'
include { pyclonevi_write_results_file } from '../pyclone-vi/pyclone-vi.nf'

// Cleaning
include { clean_work_files as clean_trimmed_fastqs } from '../utilities/utilities.nf'


workflow manifest_to_cnas {
// require:
//   MANIFEST
//   params.onco$manifest_to_cnas$fq_trim_tool
//   params.onco$manifest_to_cnas$fq_trim_tool_parameters
//   params.onco$manifest_to_cnas$aln_tool
//   params.onco$manifest_to_cnas$aln_tool_parameters
//   params.onco$manifest_to_cnas$cna_tool
//   params.onco$manifest_to_cnas$cna_tool_parameters
//   params.onco$manifest_to_cnas$aln_ref
//   params.onco$manifest_to_cnas$bed
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    cna_tool
    cna_tool_parameters
    aln_ref
    bed
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_cnas(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      cna_tool,
      cna_tool_parameters,
      aln_ref,
      bed,
      manifest)
  emit:
    cnas = raw_fqs_to_cnas.out.cnas
}

workflow raw_fqs_to_cnas {
// require:
//   FQS
//   params.onco$raw_fqs_to_cnas$fq_trim_tool
//   params.onco$raw_fqs_to_cnas$fq_trim_tool_parameters
//   params.onco$raw_fqs_to_cnas$aln_tool
//   params.onco$raw_fqs_to_cnas$aln_tool_parameters
//   params.onco$raw_fqs_to_cnas$cna_tool
//   params.onco$raw_fqs_to_cnas$cna_tool_parameters
//   params.onco$raw_fqs_to_cnas$aln_ref
//   params.onco$raw_fqs_to_cnas$bed
//   MANIFEST
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    cna_tool
    cna_tool_parameters
    aln_ref
    bed
    manifest
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_cnas(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      aln_tool,
      aln_tool_parameters,
      cna_tool,
      cna_tool_parameters,
      aln_ref,
      bed,
      manifest)
  emit:
    cnas = procd_fqs_to_cnas.out.cnas
}

workflow procd_fqs_to_cnas {
// require:
//   PROCD_FQS
//   params.onco$raw_fqs_to_cnas$aln_tool
//   params.onco$raw_fqs_to_cnas$aln_tool_parameters
//   params.onco$raw_fqs_to_cnas$cna_tool
//   params.onco$raw_fqs_to_cnas$cna_tool_parameters
//   params.onco$raw_fqs_to_cnas$aln_ref
//   params.onco$raw_fqs_to_cnas$bed
//   MANIFEST
  take:
    procd_fqs
    aln_tool
    aln_tool_parameters
    cna_tool
    cna_tool_parameters
    aln_ref
    bed
    manifest
  main:
    procd_fqs_to_alns(
      procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      params.dummy_file)
    // Cleaning upstream input intermediates
    procd_fqs
      .concat(procd_fqs_to_alns.out.alns)
      .groupTuple(by: [0, 1, 2], size: 2)
      .flatten()
      .filter{ it =~ /trimmed.fastq.gz$|trimmed.fq.gz$/ }
      .set { procd_fqs_done_signal }
//    clean_trimmed_fastqs(
//      procd_fqs_done_signal)
    alns_to_cnas(
      procd_fqs_to_alns.out.alns,
      cna_tool,
      cna_tool_parameters,
      aln_ref,
      bed,
      manifest)
  emit:
    cnas = alns_to_cnas
}


workflow alns_to_cnas {
  take:
    alns
    cna_tool
    cna_tool_parameters
    aln_ref
    bed
    manifest
  main:
    samtools_index(
      alns,
      '')
    //A lot of filtering, joining, etc. to ensure inputs go in as expected.
    manifest.filter{ it[5] =~ /TRUE/ }.set{ norms }
    manifest.filter{ it[5] =~ /FALSE/ }.set{ tumors }
    alns
      .join(samtools_index.out.bais, by: [0, 1])
      .set{ bams_bais }
    bams_bais
      .join(norms, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ norm_bams_bais }
    bams_bais
      .join(tumors, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ tumor_bams_bais }
    norm_bams_bais
      .join(tumor_bams_bais, by: [0, 2])
      .set{ norm_tumor_bams_bais }
    cna_tool_parameters = Eval.me(cna_tool_parameters)
    if( cna_tool =~ /sequenza/ ) {
      // Message
      if( params.prnt_docs ) {
        println "[raft]"
        println "[raft]CNA Tool: Sequenza"
        println "[raft]Sequenza has multiple steps:"
        println "[raft]  sequenza_gc_wiggle"
        println "[raft]  sequenza_bam2seqz"
        println "[raft]  sequenza_seqz_binning"
        println "[raft]Use a hash to define parameters for each step (e.g."
        println "[raft]cna_tool_parameters = \"[sequenza_gc_wiggle:'foo', sequenza_bam2seqz:'bar']\""
      }
      // Parameter parsing
      sequenza_gc_wiggle_parameters = cna_tool_parameters['sequenza_gc_wiggle'] ? cna_tool_parameters['sequenza_gc_wiggle'] : '-w 50'
      sequenza_bam2seqz_parameters = cna_tool_parameters['sequenza_bam2seqz'] ? cna_tool_parameters['sequenza_bam2seqz'] : ''
      sequenza_seqz_binning_parameters = cna_tool_parameters['sequenza_seqz_binning'] ? cna_tool_parameters['sequenza_seqz_binning'] : '-w 50'
      // Execution
      //A lot of filtering, joining, etc. to ensure inputs go in as expected.
      sequenza_gc_wiggle(
        aln_ref,
        sequenza_gc_wiggle_parameters)
      sequenza_bam2seqz(
         norm_tumor_bams_bais,
         sequenza_gc_wiggle.out.gc_wig,
         bed,
         sequenza_bam2seqz_parameters)
      sequenza_seqz_binning(
        sequenza_bam2seqz.out.seqz,
        sequenza_seqz_binning_parameters)
      sequenza_extract(
        sequenza_seqz_binning.out.small_seqz)
      sequenza_fit(
        sequenza_extract.out.extracts)
      sequenza_result(
        sequenza_fit.out.fits)
      sequenza_result.out.results.set{ cnas }
    }
    if( cna_tool =~ /cnvkit_batch/ ) {
      cnvkit_parameters = cna_tool_parameters['cnvkit_batch'] ? cna_tool_parameters['cnvkit_batch'] : ''
      cnvkit_batch(
        norm_tumor_bams_bais,
        bed,
        aln_ref,
        cnvkit_parameters)
    }
  emit:
    cnas = sequenza_result.out.segments_and_solutions
}

workflow cnas_and_vcfs_to_ccfs {
  take:
    cnas
    cna_tool
    targ_vcfs
    af_vcfs
    ccf_tool
    ccf_tool_parameters
  main:
    ccf_tool_parameters = Eval.me(ccf_tool_parameters)
    if( ccf_tool  =~ /pyclone-vi/ ) {
      if ( cna_tool =~ /sequenza/ ) {
        lenstools_make_pyclonevi_inputs_parameters = ccf_tool_parameters['lenstools_make_pyclonevi_input_parameters'] ? ccf_tool_parameters['lenstools_make_pyclonevi_input_parameters'] : ''
        pyclonevi_fit_parameters = ccf_tool_parameters['pyclonevi_fit'] ? ccf_tool_parameters['pyclonevi_fit'] : ''
        pyclonevi_write_results_file_parameters = ccf_tool_parameters['pyclonevi_write_results_file'] ? ccf_tool_parameters['pyclonevi_write_results_file'] : ''

        targ_vcfs
          .join(af_vcfs, by:[0,1, 2, 3])
          .join(cnas, by: [0, 1, 2, 3])
          .set{ joint_pcvi }

        lenstools_make_pyclonevi_inputs(
          joint_pcvi,
          lenstools_make_pyclonevi_inputs_parameters)

        pyclonevi_fit(
          lenstools_make_pyclonevi_inputs.out.pcvi_inputs,
          pyclonevi_fit_parameters)

        pyclonevi_write_results_file(
          pyclonevi_fit.out.pcvi_tmps,
          pyclonevi_write_results_file_parameters)
      }
    }
  emit:
    ccfs = pyclonevi_write_results_file.out.pcvi_results
}
